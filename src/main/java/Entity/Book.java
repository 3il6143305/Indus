package Entity;

import java.time.LocalDate;
import java.util.Objects;

public class Book {
    private int id;
    private String title;
    private LocalDate releaseDate;
    /**
     * Constructs a new Book instance with the specified ID, title, and release date.
     *
     * @param id the unique identifier for the book
     * @param title the title of the book
     * @param releaseDate the release date of the book
     */
    public Book(int id, String title, LocalDate releaseDate) {
        this.id = id;
        this.title = title;
        this.releaseDate = releaseDate;
    }

    // Getters and setters
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }
    public LocalDate getReleaseDate() { return releaseDate; }
    public void setReleaseDate(LocalDate releaseDate) { this.releaseDate = releaseDate; }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Book book = (Book) obj;
        return id == book.id &&
                Objects.equals(title, book.title) &&
                Objects.equals(releaseDate, book.releaseDate);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id, title, releaseDate);
    }
}