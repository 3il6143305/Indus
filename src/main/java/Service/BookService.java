package Service;

import Entity.Book;
import Repository.BookRepository;
import java.sql.SQLException;
import java.util.List;

public class BookService {
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public void addBook(Book book) throws SQLException {
        bookRepository.addBook(book);
    }

    public Book getBook(int id) throws SQLException {
        return bookRepository.getBook(id);
    }

    public void updateBook(Book book) throws SQLException {
        bookRepository.updateBook(book);
    }

    public void deleteBook(int id) throws SQLException {
        bookRepository.deleteBook(id);
    }

    public List<Book> getAllBooks() throws SQLException {
        return bookRepository.getAllBooks();
    }
}

