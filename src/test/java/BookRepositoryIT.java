import Entity.Book;
import Repository.BookRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public class BookRepositoryIT {
    private BookRepository bookRepository;

    @Before
    public void setUp() throws SQLException {
        bookRepository = new BookRepository();
        bookRepository.createTable(); // Create the books table
    }

    @After
    public void tearDown() throws SQLException {
        bookRepository.resetTable(); // Reset the books table
    }

    @Test
    public void testAddAndGetBook() throws SQLException {
        LocalDate releaseDate = LocalDate.now();
        Book newBook = new Book(1, "Test Book", releaseDate);
        bookRepository.addBook(newBook);

        Book retrievedBook = bookRepository.getBook(1);
        Assert.assertNotNull(retrievedBook);
        Assert.assertEquals("Test Book", retrievedBook.getTitle());
        Assert.assertEquals(releaseDate, retrievedBook.getReleaseDate());
    }

    @Test
    public void testUpdateBook() throws SQLException {
        LocalDate releaseDate = LocalDate.now();
        Book bookToUpdate = new Book(2, "Original Title", releaseDate);
        bookRepository.addBook(bookToUpdate);

        bookToUpdate.setTitle("Updated Title");
        bookRepository.updateBook(bookToUpdate);

        Book updatedBook = bookRepository.getBook(2);
        Assert.assertNotNull(updatedBook);
        Assert.assertEquals("Updated Title", updatedBook.getTitle());
    }

    @Test
    public void testDeleteBook() throws SQLException {
        LocalDate releaseDate = LocalDate.now();
        Book bookToDelete = new Book(3, "Delete Title", releaseDate);
        bookRepository.addBook(bookToDelete);

        bookRepository.deleteBook(3);
        Book deletedBook = bookRepository.getBook(3);
        Assert.assertNull(deletedBook);
    }

    @Test
    public void testGetAllBooks() throws SQLException {
        LocalDate releaseDate = LocalDate.now();
        bookRepository.addBook(new Book(4, "Book 1", releaseDate));
        bookRepository.addBook(new Book(5, "Book 2", releaseDate));

        List<Book> allBooks = bookRepository.getAllBooks();
        Assert.assertTrue(allBooks.size() >= 2);

        for (Book book : allBooks) {
            Assert.assertTrue(book.getTitle().equals("Book 1") || book.getTitle().equals("Book 2"));
        }
    }
}
