import org.junit.Test;

import java.time.LocalDate;

public class BookServiceTest {

    private void assertReleaseDateNotFuture(LocalDate releaseDate) {
        LocalDate currentDate = LocalDate.now();
        if (releaseDate.isAfter(currentDate)) {
            throw new IllegalArgumentException("Release date should not be in the future.");
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReleaseDateNotInFuture() {
        LocalDate futureDate = LocalDate.now().plusDays(1); // One day in the future
        assertReleaseDateNotFuture(futureDate); // This should throw an exception
    }
}
